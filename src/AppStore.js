import { Store } from 'laco';

export const AppStore = new Store({
    postList: [],
    isAddDialogOpen: false,
    postObj: {
        title: '',
        body: ''
    }
}, 'AppStore')