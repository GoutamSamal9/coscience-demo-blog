import React from "react";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import HomePage from "./pages/HomePage";
import Post from "./pages/Post";
import AddDialog from "./components/AddDialog";



function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <HomePage />
        </Route>
        <Route exact path="/post/:id">
          <Post />
        </Route>
      </Switch>
      <AddDialog />
    </Router>

  );
}

export default App;
