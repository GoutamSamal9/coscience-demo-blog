import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { Box } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';

const useStyles = makeStyles((theme) => ({
    root: {
        minWidth: 275,
        boxShadow: "0px 1px 6px -1px rgb(0 0 0 / 20%), 2px 1px 4px 0px rgb(0 0 0 / 14%), -1px 1px 5px 0px rgb(0 0 0 / 12%)",
        paddingBottom: 10,
        '&:hover': {
            background: "#dcdcdc5c",
        },
    },
    title: {
        fontSize: 14,
    },
    tittleName: {
        height: 18,
        display: '-webkit-box',
        overflow: 'hidden',
        fontSize: 16,
        fontWeight: 600,
        marginBottom: 4,
        textOverflow: 'ellipsis',
        textTransform: 'capitalize',
        justifyContent: 'space-between',
        '-webkitBoxOrient': 'vertical',
        '-webkitLineClamp': '1',
    },
    btn: {
        marginLeft: theme.spacing(1.3),
        fontSize: 10
    }
}))



export default function CardSkeleton() {

    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardContent>
                <Typography className={classes.title} color="textSecondary" gutterBottom>
                    <Skeleton variant="rect" animation="wave" />
                </Typography>
                <Box className={classes.tittleName} >
                    <Skeleton variant="rect" animation="wave" />
                </Box>
            </CardContent>
            <CardActions disableSpacing>
                <Skeleton variant="rect" className={classes.btn} width={40} animation="wave" />
            </CardActions>
        </Card>
    );
}
