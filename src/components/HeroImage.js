import React from 'react';
import { Box, Grid, makeStyles, Button } from '@material-ui/core';
import ImageHero from '../Img/HeroImg.png';
import NavBar from '../components/NavBar';

const useStyles = makeStyles((theme) => ({
    main: {
        borderBottom: '1px solid #ccc'
    },
    heroContainer: {
        width: 1370,
        height: 510,
        margin: 'auto',
    },
    textContainer: {
        width: 650,
        height: 400,
        marginTop: theme.spacing(6),
        display: 'flex',
        alignItems: 'center'
    },
    imageContainer: {
        width: 650,
        height: 400,
        display: 'flex',
        flex: '1',
        alignItems: 'center',
        marginTop: theme.spacing(6)
    },
    image: {
        width: '100%',
        borderRadius: 10,
        boxShadow: "2px 3px 11px 9px #cccccc26"
    }
}))

const HeroImage = () => {

    const classes = useStyles();

    return (
        <Box className={classes.main}>
            <NavBar />
            <Box className={classes.heroContainer}>
                <Grid container spacing={3}>
                    <Grid item lg={6}>
                        <Box className={classes.textContainer}>
                            <Box>
                                <Box fontWeight={600} fontSize={36} >Keep blogging</Box>
                                <Box fontSize={16} mt={2} >
                                    Don’t focus on having a great blog. Focus on producing a blog that’s great for your readers.
                                </Box>
                                <Box mt={2}>
                                    <Button variant="contained" color="primary" >
                                        Read Now
                                    </Button>
                                </Box>
                            </Box>
                        </Box>
                    </Grid>
                    <Grid item lg={6}>
                        <Box className={classes.imageContainer}>
                            <img alt="" src={ImageHero} className={classes.image} />
                        </Box>
                    </Grid>
                </Grid>
            </Box>
        </Box>
    )
}
export default HeroImage;