import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Box from '@material-ui/core/Box';
import { useStore } from 'laco-react';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core';
import { gql } from "@apollo/client";

import { AppStore } from '../AppStore';

const useStyles = makeStyles((theme) => ({
    btn: {
        marginRight: theme.spacing(2),
    }
}));

export default function AddDialog() {
    const classes = useStyles();

    const { isAddDialogOpen, postObj } = useStore(AppStore);



    const [errorObj, setErrorObj] = useState({});

    const validateForm = () => {

        let errorList = {};

        if (!postObj.title) {
            errorList.title = 'Please enter your title';
        }
        if (!postObj.body) {
            errorList.body = 'Please enter your  description';
        }

        if (Object.keys(errorList).length === 0) {
            setErrorObj({});
            return true;
        } else {
            setErrorObj(errorList);
            errorList = {};
            return false;
        }
    };


    const updateValue = (key, value) => {
        AppStore.set(
            () => ({ postObj: { ...postObj, [key]: value } }),
            'userObj-onchange'
        );
    };


    const handleClose = () => {
        setErrorObj({});
        AppStore.set(() => ({
            isAddDialogOpen: false
        }),
            'UserStore-data loader'
        );
    };

    const handelAdd = () => {
        if (!validateForm()) return;
        gql`{
  createPost(title: ${postObj.title},body:${postObj.body}) {
    id
    title
    body
  }
}
        `

    }

    return (
        <div>

            <Dialog
                open={isAddDialogOpen}
                onClose={handleClose}
            >
                <Box display="flex" justifyContent="space-between" >
                    <DialogTitle >Create Blog</DialogTitle>
                    <Box mt={1} mr={1}>
                        <IconButton onClick={handleClose}>
                            <CloseIcon />
                        </IconButton>
                    </Box>
                </Box>

                <Divider />
                <DialogContent>
                    <TextField
                        label="Title"
                        fullWidth
                        id="outlined-margin-dense"
                        type="mail"
                        margin="dense"
                        variant="outlined"
                        onChange={(event) => updateValue('title', event.target.value)}
                        value={postObj.title}
                        error={!!errorObj.title}
                        helperText={errorObj.title}

                    />
                    <TextField
                        label="description"
                        fullWidth
                        id="outlined-margin-dense"
                        type="mail"
                        margin="dense"
                        variant="outlined"
                        onChange={(event) => updateValue('body', event.target.value)}
                        value={postObj.body}
                        error={!!errorObj.body}
                        helperText={errorObj.body}
                        multiline
                        rows={4}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary" variant="outlined">
                        Cancel
                    </Button>
                    <Button onClick={handelAdd} className={classes.btn} color="primary" variant="contained" >
                        Save
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
