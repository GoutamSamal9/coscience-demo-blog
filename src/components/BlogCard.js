import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Box } from '@material-ui/core';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    root: {
        minWidth: 275,
        boxShadow: "0px 1px 6px -1px rgb(0 0 0 / 20%), 2px 1px 4px 0px rgb(0 0 0 / 14%), -1px 1px 5px 0px rgb(0 0 0 / 12%)",
        paddingBottom: 10,
        '&:hover': {
            background: "#dcdcdc5c",
        },
    },
    title: {
        fontSize: 14,
    },
    tittleName: {
        height: 18,
        display: '-webkit-box',
        overflow: 'hidden',
        fontSize: 16,
        fontWeight: 600,
        marginBottom: 4,
        textOverflow: 'ellipsis',
        textTransform: 'capitalize',
        justifyContent: 'space-between',
        '-webkitBoxOrient': 'vertical',
        '-webkitLineClamp': '1',
    },
    btn: {
        marginLeft: theme.spacing(1.3),
        fontSize: 10
    }
}))



export default function BlogCard({ each }) {

    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardContent>
                <Typography className={classes.title} color="textSecondary" gutterBottom>
                    Title
                </Typography>
                <Box className={classes.tittleName} title={each.title} >
                    {each.title}
                </Box>
            </CardContent>
            <CardActions disableSpacing>
                <Link to={`/post/${each.id}`} style={{ textDecoration: 'none' }} >
                    <Button
                        size="small"
                        color="primary"
                        variant="outlined"
                        className={classes.btn}
                    >
                        Read More
                    </Button>
                </Link>

            </CardActions>
        </Card>
    );
}
