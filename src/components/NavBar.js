import React from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import { AppStore } from '../AppStore';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    appContainer: {
        boxShadow: 'none',
        borderBottom: '1px solid #ccc'
    },
    title: {
        flexGrow: 1,
    },
}));

export default function NavBar() {

    const classes = useStyles();

    const handelCreatePost = () => {
        AppStore.set(() => ({
            isAddDialogOpen: true
        }),
            'UserStore-data loader'
        );

    }

    return (
        <div className={classes.root}>
            <AppBar position="static" color="inherit" className={classes.appContainer} >
                <Toolbar>
                    <Link to="/" style={{ textDecoration: 'none' }} >
                        <Typography variant="h6" className={classes.title}>
                            BLOG
                        </Typography>
                    </Link>
                    <Box style={{ marginLeft: 'auto' }} >
                        <Link to="/" style={{ textDecoration: 'none' }} >
                            <Button color="primary" >Home</Button>
                        </Link>
                        <Button onClick={handelCreatePost} color="primary">Create post</Button>
                    </Box>
                </Toolbar>
            </AppBar>
        </div>
    );
}
