import React from "react";
import { Card, CardContent, Typography, makeStyles } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';


const useStyles = makeStyles((theme) => ({

    root: {
        width: 575,
        boxShadow: "2px 3px 11px 9px #cccccc26"
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginTop: theme.spacing(2),
        marginBottom: 2,
    },
    footer: {
        bottom: '0',
        position: 'absolute',
        right: '0',
        left: '0'
    }
}))


function PostSkeleton() {

    const classes = useStyles();



    return (
        <>
            <Card className={classes.root}>
                <CardContent>
                    <Typography className={classes.title} color="textSecondary" gutterBottom>
                        <Skeleton animation="wave" variant="rect" width={100} height={10} />
                    </Typography>
                    <Typography variant="h5" component="h2">
                        <Skeleton animation="wave" variant="rect" width={100} height={15} />
                    </Typography>
                    <Typography className={classes.pos} color="textSecondary">
                        <Skeleton animation="wave" variant="rect" width={110} height={20} />
                    </Typography>
                    <Typography variant="body2" component="p">
                        <Skeleton animation="wave" variant="rect" height={50} />
                    </Typography>
                </CardContent>
            </Card>
        </>
    );
}
export default PostSkeleton;