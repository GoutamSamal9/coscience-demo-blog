import React from 'react';
import { Box, makeStyles } from '@material-ui/core';


const useStyles = makeStyles((theme) => ({
    footerContainer: {
        marginTop: theme.spacing(4),
        borderTop: '1px solid #ccc',
        padding: theme.spacing(2),
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconContainer: {
        display: 'flex',
        justifyContent: 'space-evenly',
        margin: theme.spacing(2)
    },
    eachIcon: {
        border: '1px solid #000',
        width: 35,
        height: 35,
        borderRadius: 9,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    }
}));

let footerData = [
    {
        icon: "fb"
    },
    {
        icon: "in"
    },
    {
        icon: "t"
    },
    {
        icon: "i"
    },
]



const Footer = () => {

    const classes = useStyles();

    return (
        <Box className={classes.footerContainer}>
            <Box>
                <Box className={classes.iconContainer}>
                    {footerData.map((each, i) => (
                        <Box className={classes.eachIcon} key={i} >
                            {each.icon}
                        </Box>
                    ))
                    }
                </Box>
                <Box fontSize={14}>Developed by @goutamsamal9</Box>
            </Box>
        </Box>
    )
}
export default Footer;