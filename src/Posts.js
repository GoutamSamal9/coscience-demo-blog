import React from 'react';
import { Box, Grid, makeStyles } from '@material-ui/core';
import { useQuery, gql } from "@apollo/client";

import BlogCard from './components/BlogCard';
import CardSkeleton from './components/CardSkeliton';



const useStyles = makeStyles((theme) => ({
    main: {
        width: 1370,
        margin: 'auto',
    },
    title: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        fontWeight: 600,
        fontSize: 28,
        margin: theme.spacing(3, 0)
    }
}))

const FILMS_QUERY = gql`
  query (
  $options: PageQueryOptions,
) {
  posts(options: $options) {
    data {
      id
      title
    }
    meta {
      totalCount
    }
  }
}
`;

const Posts = () => {

    const classes = useStyles();

    const { data, loading, error } = useQuery(FILMS_QUERY);


    if (error) return <pre>{error.message}</pre>


    const skeletonArray = Array(100).fill('');



    return (
        <Box className={classes.main}>
            <Box className={classes.title}>
                Blogs
            </Box>
            <Grid container spacing={3}>
                {loading ?
                    skeletonArray.map((each) =>
                        <Grid item lg={4} >
                            <CardSkeleton />
                        </Grid>)
                    :
                    <>
                        {
                            data.posts.data.map((each) => (
                                <Grid item lg={4} key={each.id}>
                                    <BlogCard each={each} />
                                </Grid>
                            ))
                        }
                    </>
                }
            </Grid>
        </Box>
    )
}
export default Posts;