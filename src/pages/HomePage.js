import React from 'react';
import { Box } from '@material-ui/core';

import HeroImage from '../components/HeroImage';
import Posts from '../Posts';
import Footer from '../Footer';


const HomePage = () => {
    return (
        <Box>
            <HeroImage />
            <Posts />
            <Footer />
        </Box>
    )
}
export default HomePage;