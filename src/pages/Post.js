import React from "react";
import { useParams } from 'react-router-dom';
import { useQuery, gql } from "@apollo/client";
import { Box, Card, CardContent, Typography, makeStyles } from '@material-ui/core';

import NavBar from "../components/NavBar";
import Footer from "../Footer";
import PostSkeleton from "../components/PostSkeliton";

const useStyles = makeStyles((theme) => ({
  main: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 400,
  },
  root: {
    width: 575,
    boxShadow: "2px 3px 11px 9px #cccccc26"
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginTop: theme.spacing(2),
    marginBottom: 2,
  },
  footer: {
    bottom: '0',
    position: 'absolute',
    right: '0',
    left: '0'
  }
}))


function Post() {

  const classes = useStyles();

  let { id } = useParams();

  const FILMS_QUERY = gql`query  {
  post (id: ${id}) {
    id
    title
    body
  }
}
  `;

  const { data, loading, error } = useQuery(FILMS_QUERY);
  if (error) return <pre>{error.message}</pre>



  return (
    <>
      <NavBar />
      <Box className={classes.main}>
        {
          loading ?
            <PostSkeleton />
            :
            <Card className={classes.root}>
              <CardContent>
                <Typography className={classes.title} color="textSecondary" gutterBottom>
                  Title
                </Typography>
                <Typography variant="h5" component="h2">
                  {data.post.title}
                </Typography>
                <Typography className={classes.pos} color="textSecondary">
                  Descriptions
                </Typography>
                <Typography variant="body2" component="p">
                  {data.post.body}
                </Typography>
              </CardContent>
            </Card>
        }

      </Box>
      <Box className={classes.footer}>
        <Footer />
      </Box>

    </>
  );
}
export default Post;